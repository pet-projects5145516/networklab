from math import e
import math;

x = 9
y = 8
z = 2


# Вводные данные
S = z/5 + 0.5
M = 2*x+y+z
lambda_med = 3*x + 2*y -z + 10


B = 10**7
V = 2.3e5
np=2
Lp=14
L_И=1600
Lc=320
vi=1
vc=0
b = 2

d = 48
h = 22


print(f"Число xyz = {x}{y}{z}")
print(f"Протяженность сети {S} км")
print(f"Число станций {M}")
print(f"Среднее значение интерсивности сообщений: {lambda_med}")
print(f"Скорость модуляции B {B}")

t = S / (V * M)
print(f"1. Время распространения сигнала между двумя соседними станциями: " +
      f"{t*10**6:.2f} мкс")

_t_cp = L_И / B
print(f"2. Средняя	длительность сообщений: " +
      f"{_t_cp*10**6:.2f} мкс")

_lambda = M * lambda_med
print(f"3. Суммарная интенсивность сообщений: " +
      f"{_lambda:.2f}")

R = _lambda*_t_cp
print(f"4. Суммарный коэффициент загрузки: " +
      f"{R:.2f}")

L_k = M*(b + B*t)
print(f"5. Эквивалентное число разрядов в кольце: " +
      f"{math.floor(L_k)}")

N = math.floor(L_k/ (h + d))
print(f"6. Допустимое число сегментов N, циркулирующих по кольцу: " +
      f"{N}")

g = L_k / N
print(f"7. Эквивалентное число разрядов сегмента с учетом разделительных разрядов: " +
      f"{g:.2f}")


C = d / g
print(f"8. Пропускная способность сети: " +
      f"{C:.2f}")


nine = 1 / (C - R)
print(f"9. Нормированное время доставки сообщения: " +
      f"{nine:.4f}")

_t_n = nine * _t_cp
print(f"10. Время доставки сообщения: " +
      f"{_t_n*10**6:.4f} мкс")


t_min = _t_cp / C
print(f"11. Минимальное время доставки сообщения: " +
      f"{t_min*10**6:.4f} мкс")








